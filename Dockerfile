FROM php:8.1-apache

RUN a2enmod rewrite
USER www-data
COPY index.php .htaccess /var/www/html/
