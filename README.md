# Time Capsule Website

An online time capsule, specifically for images and associated commentary in plain text.
Create some images, share them with someone else, and they will only be able to see them after the allotted time has passed.

Operates as a single PHP file, for maximum compatibility and ease of use on a wide range of hosting providers (The composer/docker/etc files are all for development, only the `index.php` + your own data for sharing is required).

# How to Use

## Quick Guide

Copy `./data.example` to `./data` and run the website in order to get up and running with an example time capsule.

## Detailed guide 

This time capsule is configured by putting things in the `./data` directory.

Child folders for each date are created in the format `./data/YYYY-MM-DD`.

Within each child folder, a folder should be created for each person wanting to contribute, e.g. `./data/YYYY-MM-DD/person-name`.

Each person's folder should contain one image and optionally one `.txt` file.

A `./data/config.json` file is used to specify the title of the website, along with other config items.