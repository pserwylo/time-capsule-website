<?php

class Config
{

    private static array $defaultValidImageFileExtensions = [
        'png', 'jpg', 'jpeg', 'svg', 'webp', "gif",
    ];

    private ?array $values = null;

    public function __construct()
    {
        $configFile = __DIR__ . '/data/config.json';

        if (file_exists($configFile)) {
            $contents = file_get_contents($configFile);
            $this->values = json_decode($contents, true);
        }
    }

    private function value(array $keys): mixed
    {
        $envVar = 'TIME_CAPSULE_' . join('_', array_map(fn(string $key) => strtoupper($key), $keys));
        $envValue = getenv($envVar);
        if ($envValue) {
            return $envValue;
        }

        if (!$this->values) {
            throw new RuntimeException("Tried to lookup config value, but env var $envVar not present, and can't find a config.json file.");
        }

        $value = $this->values;
        foreach ($keys as $key) {
            if (!array_key_exists($key, $value)) {
                break;
            }

            $value = $value[$key];
        }

        if ($value) {
            return $value;
        }

        throw new RuntimeException("Unable to find config value. Looked for env var $envVar and config.json entry \"" . join('.', $keys) . "\".");
    }

    private function arrayVal(array $keys): array
    {
        $value = $this->value($keys);
        if (is_array($value)) {
            return $value;
        }

        return explode(',', $value);
    }

    private function stringVal(array $keys): string
    {
        $value = $this->value($keys);

        if (is_array($value)) {
            $key = (string)array_rand($value);
            return $value[$key];
        }

        return (string)$value;
    }

    public function title(): string
    {
        return self::stringVal(['content', 'title']);
    }

    public function titlePre(): string
    {
        return self::stringVal(['content', 'title_pre']);
    }

    public function titlePost(): string
    {
        return self::stringVal(['content', 'title_post']);
    }

    /**
     * @return string[]
     */
    public function validImageFileExtensions(): array
    {
        try {
            return $this->arrayVal(['image_extensions']);
        } catch (RuntimeException) {
            return self::$defaultValidImageFileExtensions;
        }
    }

}

class TimePoint
{

    private static ?array $all = null;

    /**
     * @return TimePoint[]|null
     */
    public static function createAll(): ?array
    {
        if (self::$all === null) {
            $dataDir = __DIR__ . '/data/';

            if (!file_exists($dataDir) && !mkdir($dataDir) && !is_dir($dataDir)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $dataDir));
            }

            $timePoints = [];
            foreach(new DirectoryIterator($dataDir) as $dateDir) {
                if ($dateDir->isDot() || !$dateDir->isDir()) {
                    continue;
                }

                $date = @DateTime::createFromFormat('Y-m-d', $dateDir->getFilename());
                if ($date === false) {
                    continue;
                }

                $date->setTime(0, 0);

                $people = [];
                foreach(new DirectoryIterator($dateDir->getPathname()) as $personDir) {
                    if ($personDir->isDot() || !$personDir->isDir()) {
                        continue;
                    }

                    $people[] = new Person($personDir->getFilename());
                }

                if ($people) {
                    usort($people, fn(Person $one, Person $two) => strcmp($one->name(), $two->name()));
                    $timePoints[] = new self($date, $people);
                }
            }

            usort($timePoints, fn(TimePoint $one, TimePoint $two) => $one->date()->getTimestamp() > $two->date()->getTimestamp() ? 1 : -1);

            self::$all = $timePoints;
        }
        return self::$all;
    }

    public static function first()
    {
        $points = self::createAll();
        return $points[0];
    }

    /**
     * @param string[] $path
     */
    public static function matchingPath(array $path): ?TimePoint
    {
        foreach(self::createAll() as $timePoint) {
            if ($timePoint->date()->format('Y-m-d') === $path[0]) {
                return $timePoint;
            }
        }
        return null;
    }

    private DateTime $date;
    private array $people;

    /**
     * @param DateTime $date
     * @param Person[] $people
     */
    public function __construct(DateTime $date, array $people)
    {
        $this->date   = $date;
        $this->people = $people;
    }

    public function date(): DateTime
    {
        return $this->date; 
    }

    /**
     * @return Person[] 
     */
    public function people(): array
    {
        return $this->people; 
    }

    public function isViewable(): bool
    {
        return $this->date()->getTimestamp() <= time();
    }

}

class Person
{

    public static function matchingPath(array $path, array $people): ?Person
    {
        foreach($people as $person) {
            if ($person->name() === $path[1]) {
                return $person;
            }
        }

        return null;
    }

    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name; 
    }

}

abstract class Page
{

    /**
     * @return string[]
     */
    private static function pathParts(): array
    {
        $path = $_SERVER['REQUEST_URI'];
        $path = trim($path, "/");
        $path = $path === "index.php" ? '' : $path;
        $parts = explode('/', $path);
        if (count($parts) === 1 && $parts[0] === '') {
            $parts = array();
        }
        return $parts;
    }

    public static function createFromRequest(): self
    {
        $path = self::pathParts();
        $timePoint = (count($path) === 0) ? TimePoint::first()      : TimePoint::matchingPath($path);
        $person    = (count($path) < 2)   ? $timePoint->people()[0] : Person::matchingPath($path, $timePoint->people());

        if (count($path) === 2 || count($path) === 3) {
            $found = false;
            if ($person) {
                foreach($timePoint->people() as $p) {
                    if ($p->name() === $person->name()) {
                        $found = true;
                        break;
                    }
                }
            }

            if (!$found) {
                return new RedirectPage(new TimePointPage($timePoint, $timePoint->people()[0]));
            }
        }

        if (count($path) === 3) {

            if ($timePoint->isViewable()) {
                return new ImagePage($timePoint, $person);
            }

            return new ForbiddenImagePage($timePoint);

        }

        return new TimePointPage($timePoint, $person);
    }

    public function run(Config $config)
    {
        $this->render($config);
    }

    abstract protected function path();

    abstract protected function render(Config $config);

}

trait HasPerson
{
    private Person $person;
}

trait HasTimePoint
{
    private TimePoint $time;
}

abstract class HtmlPage extends Page
{

    final protected function render(Config $config)
    {
        header("HTTP/1.1 {$this->status()}");
        echo <<<html
<html lang="en">
	<head>
		<style>
			body, html {
				padding: 0 !important;
				margin: 0 !important;
			}

			#content {
				color: #444;
				text-align: center;
				max-width: 1024px;
				background: #CFF09E;
				box-shadow: #222 0 0 3px;
				min-height: 100%;
				margin: 0 auto;
				font-family: 'BloggerSansRegular', sans-serif;
				padding-bottom: 2em;
			}

			h1 {
				padding: 0.7em;
				background: #79BD9A;
				margin: 0;
				font-size: 3em;
			}

			h1 .before, h1 .after {
				font-size: 0.3em;
			}

			h3 {
				font-size: 1.6em;
				padding: 1em 0;
			}

			#years {
				padding-top: 0.6em;
				padding-bottom: 0.2em;
			}

			#people {
				padding-top: 0.2em;
				padding-bottom: 0.6em;
			}

			#years, #people {
				font-size: 1.2em;
				background: #A8DBA8;
			}

			#years a, #people a {
				color: #888;
				font-size: 0.8em;
			}

			#years .selected, #people .selected {
				font-weight: bold;
			}
			
			.note {
			    margin: 2em auto 0 auto;
				max-width: 70%;
				text-align: center;
			}

			#image-wrapper {
				margin-top: 2em;
			}

			#image-wrapper img {
				max-width: 70%;
				text-align: center;
				box-shadow: #000 0 0 3px;
			}

			.time-box-wrapper {
				color: white;
			}

			.time-box {
				display: inline-block;
				width: 15%;
				text-align: center;
				margin: 0.2em;
				box-shadow: #222 0 0 2px;
			}

			.time-box .time {
				font-size: 3em;
				padding: 0.4em 0.2em;
				background: #0B486B;
			}

			.time-box .unit {
				font-size: 1.5em;
				padding: 0.4em 0.2em;
				background: #3B8686;
			}

			@media screen and (max-width: 768px) {

				h3 {
					font-size: 1em;
				}

				.time-box {
				}

				.time-box .time {
					font-size: 1.5em;
					padding: 1em 0.2em;
				}

				.time-box .unit {
					font-size: 1em;
				}

				#image-wrapper img {
					max-width: 100%;
					text-align: center;
					box-shadow: #000 0 0 3px;
				}

				#image-wrapper {
					margin-top: 0;
				}

			}
		</style>
		<title></title>
	</head>
	<body>
		<div id="content">
			{$this->html($config)}
		</div>
	 	<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/blogger-sans" type="text/css"/>
	</body>
</html>
html;
    }

    abstract protected function html(Config $config);

    protected function status(): string
    {
        return '200 OK';
    }
}

class Note
{

    private string $dir;

    public function __construct(TimePoint $time, Person $person)
    {
        $webrootDir = __DIR__ . '/';
        $dataDir    = $webrootDir . 'data/';
        $this->dir  = $dataDir . $time->date()->format('Y-m-d') . '/' . $person->name();
    }

    public function text(): string|null
    {
        $path = $this->notePath();
        if (!$path || !file_exists($path)) {
            return null;
        }

        return file_get_contents($path) ?: null;
    }

    private function notePath(): ?string
    {
        if (file_exists($this->dir)) {
            foreach(new DirectoryIterator($this->dir) as $file) {
                if ($file->isDot() || $file->isDir() || $file->getExtension() !== 'txt') {
                    continue;
                }
                return $file->getPathname();
            }
        }

        return null;
    }

}

class RedirectPage extends Page
{

    private TimePointPage $page;

    public function __construct(TimePointPage $page)
    {
        $this->page = $page;
    }

    protected function path()
    {
        return $this->page->path();
    }

    protected function render(Config $config)
    {
        header('Location: ' . $this->page->path());
    }
}

class ImagePage extends Page
{

    use HasTimePoint;
    use HasPerson;

    public function __construct(TimePoint $time, Person $person)
    {
        $this->time = $time;
        $this->person = $person;
    }

    private function imgPath(array $validExtensions): ?string
    {
        $webrootDir = __DIR__ . '/';
        $dataDir    = $webrootDir . 'data/';
        $personDir  = $dataDir . $this->time->date()->format('Y-m-d') . '/' . $this->person->name();
        if (file_exists($personDir)) {
            foreach(new DirectoryIterator($personDir) as $file) {
                if ($file->isDot() || $file->isDir() || $file->getExtension() === 'txt') {
                    continue;
                }

                $extension = strtolower($file->getExtension());
                if (in_array($extension, $validExtensions)) {
                    return $file->getPathname();
                }
            }
        }
        return null;
    }

    protected function render(Config $config)
    {
        $imgPath = $this->imgPath($config->validImageFileExtensions());

        if (!$imgPath) {
            $notFoundImg = <<<svg
<svg width="639.998" height="375.387" viewBox="0 0 744.092 823.006" xmlns="http://www.w3.org/2000/svg"><g style="stroke-width:.151438"><path style="fill:#79bd9a;fill-opacity:1;stroke:#17856c;stroke-width:.249244;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M218.035 446.576h212.734v124.096H218.035z" transform="matrix(6.58807 0 0 6.61871 -1765.138 -2954.932)"/><text xml:space="preserve" style="font-style:normal;font-weight:400;font-size:14.9089px;line-height:125%;font-family:sans-serif;text-align:center;letter-spacing:0;word-spacing:0;text-anchor:middle;fill:#023a2f;fill-opacity:1;stroke:none;stroke-width:.151438px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" x="325.522" y="474.768" transform="matrix(6.58807 0 0 6.61871 -1765.138 -2954.932)"><tspan x="325.522" y="474.768" style="font-style:normal;font-variant:normal;font-weight:700;font-stretch:normal;font-size:17.5px;font-family:'DejaVu Sans';-inkscape-font-specification:'DejaVu Sans Bold';fill:#023a2f;fill-opacity:1;stroke:none;stroke-width:.151438px;stroke-opacity:1">Hmmmm...</tspan><tspan x="325.522" y="512.04" style="font-style:normal;font-variant:normal;font-weight:400;font-stretch:normal;font-size:13.75px;font-family:'DejaVu Sans';-inkscape-font-specification:'DejaVu Sans';fill:#023a2f;fill-opacity:1;stroke:none;stroke-width:.151438px;stroke-opacity:1">It appears this portrait</tspan><tspan x="325.522" y="530.676" style="font-style:normal;font-variant:normal;font-weight:400;font-stretch:normal;font-size:13.75px;font-family:'DejaVu Sans';-inkscape-font-specification:'DejaVu Sans';fill:#023a2f;fill-opacity:1;stroke:none;stroke-width:.151438px;stroke-opacity:1">hasn&apos;t been put in the</tspan><tspan x="325.522" y="549.312" style="font-style:normal;font-variant:normal;font-weight:400;font-stretch:normal;font-size:13.75px;font-family:'DejaVu Sans';-inkscape-font-specification:'DejaVu Sans';fill:#023a2f;fill-opacity:1;stroke:none;stroke-width:.151438px;stroke-opacity:1">time capsule yet.</tspan></text></g></svg>
svg;

            header('Content-Length: ' . strlen($notFoundImg));
            header('Content-Type: image/svg+xml');
            echo $notFoundImg;
            return;
        }

        header('Content-Length: ' . filesize($imgPath));
        header('Content-Type: ' . mime_content_type($imgPath));
        echo file_get_contents($imgPath);
    }

    protected function path(): string
    {
        return '/' . $this->time->date()->format('Y-m-d') . '/' . $this->person->name() . '/img';
    }
}

class TimePointPage extends HtmlPage
{

    use HasTimePoint;
    use HasPerson;

    public function __construct(TimePoint $time, Person $person)
    {
        $this->time = $time;
        $this->person = $person;
    }

    protected function html(Config $config): string
    {
        return <<<html
<h1>
	<span class="before">{$config->titlePre()}</span><br />
	{$config->title()}<br />
	<span class="after">{$config->titlePost()}</span>
</h1>
{$this->yearRow()}
{$this->personRow()}
{$this->body()}
html;
    }

    private function yearRow(): string
    {
        $links = array_map(
            function (TimePoint $time) {

                $timeString = $time->date()->format('Y');
                if ($this->time->date() == $time->date()) {
                    return "<span class='selected'>$timeString</span>";
                }

                    $page = new TimePointPage($time, $this->person);
                    return "<a href='{$page->path()}'>$timeString</a>";

            }, TimePoint::createAll()
        );

        $linksHtml = implode(" ", $links);
        return <<<html
<div id="years">In the year $linksHtml</div>
html;
    }

    private function personRow(): string
    {
        $links = array_map(
            function (Person $person) {
                if ($this->person->name() === $person->name()) {
                    return "<span class='selected'>{$person->name()}</span>";
                } else {
                    $page = new TimePointPage($this->time, $person);
                    return "<a href='{$page->path()}'>{$person->name()}</a>";
                }
            }, $this->time->people()
        );

        $linksHtml = implode(" ", $links);
        return <<<html
<div id="people">According to $linksHtml</div>
html;
    }

    private function body(): string
    {
        if ($this->time->date()->getTimestamp() > time()) {
            return $this->notReady();
        }

        return $this->img();
    }

    private function img(): string
    {
        $imgPage = new ImagePage($this->time, $this->person);
        $note = (new Note($this->time, $this->person))->text();

        $noteHtml = !$note ? '' : <<<html
<div class="note">
    $note
</div>
html;


        return <<<html
$noteHtml
<div id="image-wrapper">
	<img src="{$imgPage->path()}" alt="Image from {$this->person->name()}" />
</div>
html;
    }

    private function notReady(): string
    {
        $dateDiff = $this->time->date()->diff(new DateTime());

        return <<<html
<h3>To see this portrait from {$this->person->name()} in 2016, come back in</h3>
<div class="time-box-wrapper">
	{$this->timeBox($dateDiff->y, 'Year')}
	{$this->timeBox($dateDiff->m, 'Month')}
	{$this->timeBox($dateDiff->d, 'Day')}
	{$this->timeBox($dateDiff->h, 'Hour')}
	{$this->timeBox($dateDiff->i, 'Minute')}
	{$this->timeBox($dateDiff->s, 'Second')}
</div>
html;
    }

    private function timeBox(int $num, string $unit): string
    {
        $plural = $num === 1 ? '' : 's';
        return <<<html
<div class="time-box">
	<div class="time">$num</div>
	<div class="unit">$unit$plural</div>
</div>
html;
    }

    protected function path(): string
    {
        return '/' . $this->time->date()->format('Y-m-d') . '/' . $this->person->name();
    }
}

class ForbiddenImagePage extends HtmlPage
{
    use HasTimePoint;

    public function __construct(TimePoint $time)
    {
        $this->time = $time;
    }

    protected function status(): string
    {
        return '403 Forbidden';
    }

    protected function html(Config $config): string
    {
        return <<<html
<h1>Forbidden</h1>
<p>Can't view this image until {$this->time->date()->format('d/m/Y')}</p>
html;
    }

    protected function path(): string
    {
        return '/forbidden';
    }
}

class NotFoundPage extends HtmlPage
{
    protected function status(): string
    {
        return '404 Not Found';
    }

    protected function html(Config $config): string
    {
        return <<<html
<h1>Not Found</h1>
<p>Page not found</p>
html;
    }

    protected function path(): string
    {
        return '/not-found';
    }
}

$config = new Config();
Page::createFromRequest()->run($config);